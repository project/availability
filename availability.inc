<?php

/**
 * Inserts a set of dates of a user into the database.
 *
 * @param integer $nid Id of the parent node for dates to insert.
 * @param integer $uid Id of the dates owner.
 * @param array $dates An array of dates to insert.
 */
function availability_insert_user_dates($nid, $uid, $dates) {
  $sql = 'INSERT INTO {node_availability} (nid, uid, date) VALUES(%d, %d, %d)';

  foreach ($dates as $date) {
    db_query($sql, $nid, $uid, $date);
  }
}

/**
 * Deletes dates for the specified node and user.
 *
 * @param integeer $nid Id of the parent node for dates to read.
 * @param integeer $uid Id of the dates owner.
 */
function availability_delete_user_dates($nid, $uid) {
  $sql = 'DELETE FROM {node_availability} WHERE nid=%d AND uid=%d';
  db_query($sql, $nid, $uid);
}

/**
 * Enter description here...
 */
/*function availability_load_user_dates($nid, $uid = null) {
  $sql = "SELECT * FROM {node_availability} na WHERE na.nid='%d' ".(($uid)?"AND na.uid='%d'": "")." AND na.date > ".time();
  $result = db_query($sql, $nid, $uid);

  $dates = array();
  while ($item = db_fetch_array($result)) {
    $dates[] = date("Y-m-d", $item['date']);
  }
  return $dates;
}*/

/**
 * Loads availability information of all users for the specified node starting from today.
 *
 * @param integer $nid Node id.
 * @return array An array of loaded dates.
 */
function availability_load_all_dates($nid) {
  $sql =
    'SELECT na.nid, na.uid, na.date, u.name FROM {node_availability} na '.
    'INNER JOIN {users} u ON na.uid = u.uid WHERE na.nid = %d AND na.date > %d';

  $result = db_query($sql, $nid, time());

  $dates = array();
  while ($item = db_fetch_array($result)) {
    $dates[$item['uid']][] = $item;
  }
  return $dates;
}

/**
 * Converts string date in the format 'year-month-day' into unix timestamp.
 *
 * @param string $date String date.
 * @return Unix timestamp of the specified date.
 */
/*function availability_datetostamp($date) {
  list($year, $month, $day) = sscanf($date, '%d-%d-%d');
  return mktime(0, 0, 0, $month, $day, $year);
}*/

/**
 * Converts string date in the format 'year-month-day' into unix timestamp.
 *
 * @param integer $date Unix timestamp.
 * @param string $time A string representing the time (ex. "12:00:00").
 * @return Unix timestamp which is equal to the specified date with the specified time.
 */
function availability_change_time($date, $time) {
  $time = explode(':', $time);
  $date = getdate($date);
  $date = mktime($time[0], $time[1], $time[2], $date['mon'], $date['mday'], $date['year']);
  return $date;
}
