
Description
------------

Availability module is an abstract solution to associate a set of dates
with certain nodes and users assuming that these nodes are available
on specified dates for a certain user.

For instance, if you have a content type 'apartment' you can enable
availability support for all 'apartment' nodes and then node owners
will be able to specify which dates these apartments are available or not
available. More than, other users who are allowed to edit these apartments
can specify their own dates. So, an apartment may be avilable for User 1
on '01.01.2007' and '02.01.2007' and for User 2 on '03.01.2007' and
'04.01.2007'.


Installation
------------

To install this module, you need the following:

1. Install prerequisites: jscalendar.module (JS Tools package).

2. Copy availability module in your Drupal's modules directory.

3. Copy calendar_stripped.js and calendar-setup_stripped.js files
from 'jscalendar' subfolder of availability module over their original
versions from jscalendar module. These files are core of jscalendar library
and are usually stored in 'jstools/jscalendar/lib' directory.

4. Go to 'admin/build/modules' page and turn jstools.module, jscalendar.module,
availability.module on.

5. Configure availability.module

IMPORTANT: Availability module uses a flat mode of jscalendar component
which is NOT available in free version that's why we provide our own patched
version of jscalendar which is more powerful and also free.

Configuring
-----------

After installation visit 'admin/content/types' page, choose a desired content
type and press 'edit'. Enable availability support for this content type.

Then go to the 'admin/settings/availability' page and: 
1. set the default time which is used in all dates saved into the database by
availability module (it is important that all dates have the save time).
2. choose whether to display availability information by default on a node view
page or on a separate node page (MENU_LOCAL_TASK).
