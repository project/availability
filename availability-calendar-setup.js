var MA = [];
var AVL_MA = [];

function dateClearTime(date) {
  date.setHours(0);
  date.setMinutes(0);
  date.setSeconds(0);
  return date;
}

function datesEqual(date1, date2) {
  return (
    date1.getDate() == date2.getDate() &&
    date1.getMonth() == date2.getMonth() &&
    date1.getFullYear() == date2.getFullYear()
  );
}

function availabilityFlatCallback(cal) {
  if (cal.dateClicked || cal.params.flat == null ) {
    cal.params.displayArea.value = '';
    if (cal.multiple) {
      MA.length = 0;
      for (var i in cal.multiple) {
        var d = cal.multiple[i];
        if (d) {
          if (cal.params.displayArea.value != "") {
            cal.params.displayArea.value += "," + Math.floor(d.getTime()/1000);
          } else {
            cal.params.displayArea.value = Math.floor(d.getTime()/1000);
          }
        }
      }
    }
  }
  return true;
}
function availabilityInitSelectionDates()
{
  var r = document.getElementById('edit-daterange');
  var arr = r.value.split(",");
  for (var i in arr){
    MA[i] = new Date(arr[i]*1000);
  }

  var avl = document.getElementById('edit-avldaterange');
  var avlarr = avl.value.split(",");
  for (var i in avlarr) {
    AVL_MA[i] = new Date(avlarr[i]*1000);
  }
}

function availabilityIsDisabled(date) {
  var today = dateClearTime(new Date());

  if (dateClearTime(date) < today.getTime()) {
    return true;
  } else {
    for (var i in AVL_MA) {
      if (datesEqual(AVL_MA[i], date)) {
        return true;
      }
    }
  }

  return false;
}

if (Drupal.jsEnabled) {
  $(document).ready(function() {
    availabilityInitSelectionDates();
    $("div[@id='calendar-container-flat']").each(function () {
      var id = $(this).id();
      Calendar.setup({
        weekNumbers  : false,
        displayArea  : 'edit-daterange',
        flat         : id,
        flatCallback : availabilityFlatCallback,
        disableFunc  : availabilityIsDisabled,
        multiple     : MA
      });
    });
  });
}
